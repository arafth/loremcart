<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


//---------- Pages Controller Routes --------------------
Route::get('/', 'Frontend\PagesController@index')->name('index');
Route::get('contact', 'Frontend\PagesController@contact')->name('contact');
Route::get('search', 'Frontend\PagesController@search')->name('search');



Route::group(['prefix' => 'products'], function () {
    //---------- Product Controller Routes --------------------
    Route::get('/', 'Frontend\ProductController@product')->name('products');
    Route::get('/product/{slug}', 'Frontend\ProductController@showSingleProduct')->name('showSingleProduct');

    //-----------------  Category Controller Routes ------------
    Route::get('/categories/{slug}', 'Frontend\ProductController@showCategoryWiseProducts')->name('showCategoryWiseProducts');
});



//---------- Controller Routes for admin --------------------
Route::group(['prefix' => 'admin'], function () {

    Route::get('dashboard', 'Backend\AdminPagesController@dashboard')->name('dashboard');
    Route::get('/', 'Backend\AdminProductController@productIndex')->name('productIndex');
    Route::get('create', 'Backend\AdminProductController@create')->name('createProduct');
    Route::get('show', 'Backend\AdminProductController@show')->name('showProductList');
    Route::get('edit/{id}', 'Backend\AdminProductController@edit')->name('editProduct');

    Route::post('/store', 'Backend\AdminProductController@store')->name('storeProduct');
    Route::patch('edit/{id}', 'Backend\AdminProductController@update')->name('updateProduct');
    Route::delete('delete/{id}', 'Backend\AdminProductController@destroy')->name('deleteProduct');

//  #################  Category Controller ##############
    Route::group(['prefix' => 'category'], function () {

        Route::get('show', 'Backend\CategoryController@show')->name('showCategory');
        Route::get('create', 'Backend\CategoryController@create')->name('createCategory');
        Route::get('edit/{id}', 'Backend\CategoryController@edit')->name('editCategory');

        Route::post('/store', 'Backend\CategoryController@store')->name('storeCategory');
        Route::patch('edit/{id}', 'Backend\CategoryController@update')->name('updateCategory');
        Route::delete('delete/{id}', 'Backend\CategoryController@destroy')->name('deleteCategory');
    });

//  ################ Brand Controller ##################
    Route::group(['prefix' => 'brand'], function () {

        Route::get('show', 'Backend\BrandController@show')->name('showBrand');
        Route::get('create', 'Backend\BrandController@create')->name('createBrand');
        Route::get('edit/{id}', 'Backend\BrandController@edit')->name('editBrand');

        Route::post('/store', 'Backend\BrandController@store')->name('storeBrand');
        Route::patch('edit/{id}', 'Backend\BrandController@update')->name('updateBrand');
        Route::delete('delete/{id}', 'Backend\BrandController@destroy')->name('deleteBrand');
    });

     //  #################  Division Controller ##############
    Route::group(['prefix' => 'division'], function () {

        Route::get('show', 'Backend\DivisionController@show')->name('showDivision');
        Route::get('create', 'Backend\DivisionController@create')->name('createDivision');
        Route::get('edit/{id}', 'Backend\DivisionController@edit')->name('editDivision');

        Route::post('/store', 'Backend\DivisionController@store')->name('storeDivision');
        Route::patch('edit/{id}', 'Backend\DivisionController@update')->name('updateDivision');
        Route::delete('delete/{id}', 'Backend\DivisionController@destroy')->name('deleteDivision');
    });

    //  #################  District Controller ##############
    Route::group(['prefix' => 'district'], function () {

        Route::get('show', 'Backend\DistrictController@show')->name('showDistrict');
        Route::get('create', 'Backend\DistrictController@create')->name('createDistrict');
        Route::get('edit/{id}', 'Backend\DistrictController@edit')->name('editDistrict');

        Route::post('/store', 'Backend\DistrictController@store')->name('storeDistrict');
        Route::patch('edit/{id}', 'Backend\DistrictController@update')->name('updateDistrict');
        Route::delete('delete/{id}', 'Backend\DistrictController@destroy')->name('deleteDistrict');
    });
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

//user routes
Route::get('token/{token}', 'Frontend\EmailVerificationController@verify')->name('userVerification');
