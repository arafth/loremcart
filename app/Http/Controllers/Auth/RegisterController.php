<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Notifications\VerifyRegistration;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Request;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'first_name' => ['required', 'string', 'max:255'],
            'last_name' => ['required', 'string', 'max:255'],
            'phone_no' => ['required', 'string', 'max:255'],
            'division_id' => ['required', 'string', 'max:5'],
            'district_id' => ['required', 'string', 'max:5'],
            'street_address' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function register(Request $request)
    {
        $user = User::create([
            'first_name' => request('first_name'),
            'last_name' => request('last_name'),
            'username' => Str::slug(request('first_name').request('last_name')),
            'phone_no' => request('phone_no'),
            'division_id' => request('division_id'),
            'district_id' => request('district_id'),
            'street_address' => request('street_address'),
            'email' => request('email'),
            'password' => Hash::make(request('password')),
            'status' => 0,
            'remember_token' => Str::random(50),
            'ip_address' => Request::ip(),
        ]);

        $user->notify(new VerifyRegistration($user));

        session()->flash('Success', 'A verification message has been sent to your email. Please confirm your email before login.');
        return redirect()->route('index');
    }
}
