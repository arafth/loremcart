<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\District;
use App\Models\Division;
use Illuminate\Http\Request;

class DivisionController extends Controller
{
        /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages/admin/division/createDivision');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
        $validated_data = request()->validate([
            'name' => ['required', 'min:3']
        ], [
            'name.required' => 'Please input Division Name.'
        ]);

        Division::create($validated_data);

        session()->flash('success', 'Division has been created successfully');
        return redirect()->route('showDivision');
    }

    /**
     * Display the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        $divisions = Division::orderBy('id', 'asc')->get();
        return view('pages/admin/division/showDivision', compact('divisions'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Division $id)
    {
        session()->flash('success', 'Division has been edited successfully');
        return view('pages/admin/division/editDivision', compact('id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        request()->validate([
            'name' => ['required', 'min:3']
        ], [
            'name.required' => 'Please input Division Name.'
        ]);

        $division = Division::findOrFail($id);
        $division->name = $request->name;
        $division->save();

        session()->flash('success', 'Division has been updated successfully');
        return redirect()->route('showDivision');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $division = Division::findOrFail($id);
        if (!is_null($division)) {
            //delete all districts under this division too
            $districts = District::where('division_id', $division->id)->get();
            foreach ($districts as $district) {
                $district->delete();
            }
            $division->delete();
        }

        session()->flash('success', 'Division has been deleted successfully!');
        return back();
    }
}
