<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Product;
use App\Models\ProductImage;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Image;

class AdminProductController extends Controller
{
        /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function productIndex()
    {
        return view('/pages/admin/adminIndex');
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages/admin/products/createProduct');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */


    public function store(Request $request)
    {
        $slugGen = Str::slug(request('title'));

        // $count = Product::whereRaw("slug RLIKE '^{$slug}(-[0-9]+)?$'")->count();
        // 'slug' = $count ? "{$slug}-{$count}" : $slug;
        // dd('slug');
        request()->validate([
        'title' => ['required', 'min:5'],
        'price' => ['required', 'min:1'],
        'quantity' => ['required', 'min:1'],
        'category_id' => ['required', 'min:1'],
        'brand_id' => ['required', 'min:1'],
        'admin_id' => ['required', 'min:1'],
        'description' => ['nullable'],
        ], [
            'title.required' => 'Please provide a valid product name',
            'title.min:5' => 'Product name minimum range is 5.',
            'price.required' => 'Please provide product price. (Minimum 1 number)',
        ]);
        // dd($validated_data);
        // Product::create(['title', 'price', 'quantity', 'category_id', 'brand_id', 'admin_id', 'description', $slugGen]);

        $product = new Product;

        $product->title = $request->title;
        $product->price = $request->price;
        $product->quantity = $request->quantity;
        $product->category_id = $request->category_id;
        $product->brand_id = $request->brand_id;
        $product->admin_id = $request->admin_id;
        $product->description = $request->description;
        $product->slug = $slugGen;

        $product->save();

        //Image insertion into Prodcut Model


        if (count($request->ProductImg) > 0) {
            foreach ($request->ProductImg as $image) {
                // $image = $request->file('ProductImg');
                $imageName = time() . '.' . $image->getClientOriginalExtension();
                $location = public_path('images/Products/'.$imageName);
                // Image::make($request->file('ProductImg')->getRealPath())->save($location);
                Image::make($image)->save($location);

                $product_image = new ProductImage;
                $product_image->product_id = $product->id;
                $product_image->image = $imageName;
                $product_image->save();
            }
        }


        return redirect()->route('showProductList');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        $products = Product::orderBy('id', 'asc')->get();
        return view('pages/admin/products/showProduct', compact('products'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $id)
    {

        return view('pages/admin/products/editProduct', compact('id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Product $id, Request $request)
    {
        $validated_data = request()->validate([
        'title' => ['required', 'min:5'],
        'price' => ['required', 'min:1'],
        'quantity' => ['required', 'min:1'],
        'category_id' => ['required', 'min:1'],
        'brand_id' => ['required', 'min:1'],
        'admin_id' => ['required', 'min:1'],
        'description' => ['nullable'],
        ], [
            'title.required' => 'Please provide a valid product name',
            'title.min:5' => 'Product name minimum range is 5.',
            'price.required' => 'Please provide product price. (Minimum 1 number)',
        ]);

        // $id->update(request(['title', 'price', 'quantity', 'category_id', 'brand_id', 'admin_id', 'description',]));
        $id->update($validated_data);

        //Image update into Product Model

        return redirect()->route('showProductList');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $product = Product::findOrFail($id);
        if (!is_null($product)) {
            $product->delete();
        }

        session()->flash('success', 'Product has been deleted successfully!');
        return back();
    }
}
