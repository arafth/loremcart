<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\District;
use App\Models\Division;
use Illuminate\Http\Request;

class DistrictController extends Controller
{
        /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $divisions = Division::orderBy('id', 'asc')->get();
        return view('/pages/admin/district/createDistrict', compact('divisions'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
        $validated_data = request()->validate([
            'name' => ['required'],
            'division_id' => ['required'],
        ], [
            'name.required' => 'Please provide a valid category name',
            'division_id.required' => 'Please select a division'
        ]);

        District::create($validated_data);

        session()->flash('success', 'District has been created successfully!');
        return redirect()->route('showDistrict');
    }

    /**
     * Display the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        $districts = District::orderBy('id', 'asc')->get();
        return view('pages/admin/district/showDistrict', compact('districts'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $divisions = Division::orderBy('id', 'asc')->get();
        $district = District::findorFail($id);

        session()->flash('success', 'District has been edited successfully!');
        return view('pages/admin/district/editDistrict', compact('district', 'divisions'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $district = District::findorFail($id);

        $district->name = $request->name;
        $district->division_id = $request->division_id;

        $district->save();

        session()->flash('success', 'District has been updated successfully!');
        return redirect()->route('showDistrict');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $district = District::findorFail($id);

        if (!is_null($district)) {
            $district->delete();
        }

        session()->flash('success', 'District has been deleted successfully!');
        return redirect()->route('showDistrict');
    }
}
