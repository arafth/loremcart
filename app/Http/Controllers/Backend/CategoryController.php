<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Image;
use File;

class CategoryController extends Controller
{
        /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $mainCategory = Category::orderBy('id', 'asc')->where('parent_id', null)->get();
        return view('/pages/admin/categories/createCategory', compact('mainCategory'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $slugGen = Str::slug(request('categoryName'));
        // dd($slugGen);
        request()->validate([
            'categoryName' => ['required'],
            'categoryImg' => ['nullable','mimes:jpg,jpeg,png'],
        ], [
            'categoryName.required' => 'Please provide a valid category name',
            'categoryImg.image' => 'Please select an image file only. [.jpg, .jpeg, .png]'
        ]);

        $category = new Category;

        $category->name = $request->categoryName;
        $category->slug = $slugGen;
        $category->parent_id = $request->parent_id;
        $category->description = $request->description;



         //Image insertion into Prodcut Model


        if ($request->hasFile('categoryImg')) {
            $image = $request->file('categoryImg');
            $imageName = time() . '.' . $image->getClientOriginalExtension();
            $location = public_path('images/Categories/'.$imageName);
            // Image::make($request->file('ProductImg')->getRealPath())->save($location);
            Image::make($image)->save($location);

            $category->image = $imageName;
        }

        $category->save();

        return redirect()->route('showCategory');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        $categories = Category::orderBy('id', 'asc')->get();
        return view('pages/admin/categories/showCategory', compact('categories'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Category $id)
    {
        $mainCategory = Category::orderBy('id', 'asc')->where('parent_id', null)->get();
        return view('pages/admin/categories/editCategory', compact('id', 'mainCategory'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        // dd(count($request->categoryImg) > 0);
        request()->validate([
            'categoryName' => ['required'],
            // 'categoryImg' => ['nullable','mimes:jpg,jpeg,png'],
        ], [
            'categoryName.required' => 'Please provide a valid category name',
            // 'categoryImg.image' => 'Please select an image file only. [.jpg, .jpeg, .png, .bmp, .gif, .svg, or .webp]'
        ]);

        $category = Category::findorFail($id);

        $category->name = $request->categoryName;
        $category->parent_id = $request->parent_id;
        $category->description = $request->description;



         //Image insertion into Prodcut Model


        if ($request->hasFile('categoryImg')) {
            if (File::exists('images/Categories/'.$category->image)) {
                File::delete('images/Categories/'.$category->image);
            }

            $image = $request->file('categoryImg');
            $imageName = time() . '.' . $image->getClientOriginalExtension();
            $location = public_path('images/Categories/'.$imageName);
            // Image::make($request->file('ProductImg')->getRealPath())->save($location);
            Image::make($image)->save($location);

            $category->image = $imageName;
        }

        $category->save();

        session()->flash('success', 'Category has been updated successfully!');
        return redirect()->route('showCategory');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $category = Category::findOrFail($id);
        if (!is_null($category)) {
            //if it's a parent category, delete it's child category too
            if ($category->parent_id == null) {
                $subCategories = Category::orderBy('id', 'asc')->where('parent_id', $category->id)->get();
                foreach ($subCategories as $sub) {
                    if (File::exists('images/Categories/'.$sub->image)) {
                        File::delete('images/Categories/'.$sub->image);
                    }
                    $sub->delete();
                }
            }

            if (File::exists('images/Categories/'.$category->image)) {
                File::delete('images/Categories/'.$category->image);
            }

            $category->delete();
        }

        session()->flash('success', 'Category has been deleted successfully!');
        return back();
    }
}
