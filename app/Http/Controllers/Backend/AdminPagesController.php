<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AdminPagesController extends Controller
{
    // Display dashboard
    public function dashboard()
    {
        return view('pages/admin/adminDashboard');
    }
}
