<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Brand;
use File;
use Illuminate\Http\Request;
use Image;

class BrandController extends Controller
{
        /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('/pages/admin/brands/createBrand');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        // dd(request('BrandName'));
        request()->validate([
            'brandName' => ['required'],
            'brandImg' => ['nullable','mimes:jpg,jpeg,png'],
        ], [
            'brandName.required' => 'Please provide a valid Brand name',
            'brandImg.image' => 'Please select an image file only. [.jpg, .jpeg, .png]'
        ]);

        $brand = new Brand;

        $brand->name = $request->brandName;
        $brand->description = $request->description;



         //Image insertion into Prodcut Model

        if ($request->hasFile('brandImg')) {
            $image = $request->file('brandImg');
            $imageName = time() . '.' . $image->getClientOriginalExtension();
            $location = public_path('images/Brands/'.$imageName);
            // Image::make($request->file('ProductImg')->getRealPath())->save($location);
            Image::make($image)->save($location);

            $brand->image = $imageName;
        }

        $brand->save();

        return redirect()->route('showBrand');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        $brands = Brand::orderBy('id', 'asc')->get();
        return view('pages/admin/brands/showBrand', compact('brands'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Brand $id)
    {
        $mainBrand = Brand::orderBy('id', 'asc')->get();
        return view('pages/admin/brands/editBrand', compact('id', 'mainBrand'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        // dd(count($request->BrandImg) > 0);
        request()->validate([
            'brandName' => ['required'],
            // 'BrandImg' => ['nullable','mimes:jpg,jpeg,png'],
        ], [
            'brandName.required' => 'Please provide a valid Brand name',
            // 'BrandImg.image' => 'Please select an image file only. [.jpg, .jpeg, .png, .bmp, .gif, .svg, or .webp]'
        ]);

        $brand = Brand::findorFail($id);

        $brand->name = $request->brandName;
        $brand->description = $request->description;


         //Image insertion into Prodcut Model

        if ($request->hasFile('brandImg')) {
            if (File::exists('images/Brands/'.$brand->image)) {
                File::delete('images/Brands/'.$brand->image);
            }

            $image = $request->file('brandImg');
            $imageName = time() . '.' . $image->getClientOriginalExtension();
            $location = public_path('images/Brands/'.$imageName);
            // Image::make($request->file('ProductImg')->getRealPath())->save($location);
            Image::make($image)->save($location);

            $brand->image = $imageName;
        }

        $brand->save();

        session()->flash('success', 'Brand has been updated successfully!');
        return redirect()->route('showBrand');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $brand = Brand::findOrFail($id);
        if (!is_null($brand)) {
            if (File::exists('images/Brands/'.$brand->image)) {
                File::delete('images/Brands/'.$brand->image);
            }

            $brand->delete();
        }

        session()->flash('success', 'Brand has been deleted successfully!');
        return back();
    }
}
