<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Models\Product;
use Illuminate\Http\Request;

class PagesController extends Controller
{
    public function index(Product $products)
    {
        $products = Product::orderBy('id', 'asc')->paginate(5);
        return view('pages/frontend/index', compact('products'));
    }

    public function product(Product $products)
    {
        $products = Product::orderBy('id', 'asc')->paginate(5);
        return view('pages/frontend/products', compact('products'));
    }

    public function contact()
    {
        return view('pages/frontend/contact');
    }

    public function search(Request $request)
    {
        $searchString = $request->search;
        $products = Product::orWhere('title', 'like', '%'.$searchString.'%')
        ->orWhere('description', 'like', '%'.$searchString.'%')
        ->orWhere('slug', 'like', '%'.$searchString.'%')
        ->orWhere('price', 'like', '%'.$searchString.'%')
        ->orWhere('quantity', 'like', '%'.$searchString.'%')
        ->orderBy('id', 'asc')
        ->paginate(5);

        return view('pages/frontend/search', compact('searchString', 'products'));
    }
}
