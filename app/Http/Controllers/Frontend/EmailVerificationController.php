<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;

class EmailVerificationController extends Controller
{
    public function verify($token)
    {
        $user = User::where('remember_token', $token)->first();
        if (!is_null($user)) {
            $user->status = 1;
            $user->remember_token = null;
            $user->save();
            session()->flash('success', 'You are registered successfully. Please login now.');
            return redirect()->route('login');
        } else {
            session()->flash('errors', 'Sorry! Your token in invalid.');
            return redirect('/');
        }
    }
}
