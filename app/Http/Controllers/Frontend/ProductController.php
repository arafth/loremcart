<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Product;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    public function product(Product $products)
    {
        $products = Product::orderBy('id', 'asc')->paginate(5);
        return view('pages/frontend/products', compact('products'));
    }

    public function showSingleProduct($slug)
    {
        $product = Product::where('slug', $slug)->first();
        if (!is_null($product)) {
            return view('pages/frontend/showSingleProduct', compact('product'));
        } else {
            session()->flash('success', 'Sorry the product you are looking for is not available!');
            return redirect()->route('products');
        }
    }

    //############### Category Wise Product Display ################
    public function showCategoryWiseProducts($slug)
    {
        $slugID = Category::where('slug', $slug)->value('id');
        $sortByCategory = Product::where('category_id', $slugID)->paginate(5);
        // dd($sortByCategory);
        if (!is_null($sortByCategory)) {
            return view('pages/frontend/showCategoryWiseProducts', compact('sortByCategory', 'slug'));
        } else {
            session()->flash('errors', 'Oopss!!! Sorry..... This category is empty!');
            return redirect()->route('products');
        }
    }
}
