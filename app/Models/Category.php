<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    public function parent()
    {
        return $this->belongsTo(Category::class);  //parameter 'parent_id' omitted as laravel will auto pass this parameter
    }

    public function products()
    {
        return $this->hasMany(Product::class);
    }


// Check; the mentioned category is the child of parent or not

    public function parentOrNotCategory($parent_id, $child_id)
    {
        $categories = Category::where('id', $child_id)->where('parent_id', $parent_id)->get();
        if (!is_null($categories)) {
            return true;
        } else {
            return false;
        }
    }
}
