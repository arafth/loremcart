<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" type="image/png" href="{{ asset('images/favicon.ico')}}"/>

    @include('partials/style')
    <title>@yield('title', 'LOREM Cart')
    </title>

</head>
<body>
<div class="flex-wrap main-container">

@include('partials/nav')

@yield('content')


@include('partials/footer')

</div>


@include('partials/scripts')

</body>
</html>
