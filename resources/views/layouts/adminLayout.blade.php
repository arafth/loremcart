<!DOCTYPE html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>@yield('title', 'LOREM Cart Admin Dashboard || LOREM Cart')</title>

    @include('partials/adminStyle')


  </head>
  <body>
    <div class="container-scroller">
        @include('partials/admin_navbar')


      <div class="container-fluid page-body-wrapper">
        <!-- partial:partials/_sidebar.html -->
        @include('partials/admin_sidebar')
        <!-- partial -->
        <div class="main-panel">
            <div class="content-wrapper">
                @yield('content')

            </div>
          <!-- content-wrapper ends -->
          <!-- partial:partials/_footer.html -->
            @include('partials/admin_footer')
          <!-- partial -->
        </div>
        <!-- main-panel ends -->
      </div>
      <!-- page-body-wrapper ends -->
    </div>
    @include('partials/adminScript')

    @yield('scripts')

  </body>
</html>
