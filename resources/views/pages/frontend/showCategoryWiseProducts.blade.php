@extends('layouts/layout')

@section('title', 'Products || LOREM Cart')

@section('content')
 {{-- Start sidebar + content --}}

<div class="container-fluid ml-0 p-0 mt-20">
    <div class="row">
        <div class="col-md-3">
            @include('partials/productSidebar') {{-- sidebar inclution --}}
        </div>


        <div class="col-md-9">
            <div class="widget">
              <h3 class="badge badge-primary h4 pb-2">Products</h3>
                @include('partials/message')
              <div class="row">
                {{-- @php $products = $sortByCategory->products()->paginate(5); @endphp --}}
                @if (count($sortByCategory) > 0)

                    @foreach ($sortByCategory as $product)
                        {{-- Product card start--}}
                        <div class="col-md-3 mr-2 mb-2">
                          <div class="card card-deck">

                            @php $i = 1; @endphp

                            @foreach ($product->images as $image)
                                @if ($i > 0)
                                <a href="{{ route('showSingleProduct', $product->slug)}}">
                                  <img class="card-img-top feature-image" src="{{ asset('images/Products/'.$image->image)}}" alt="{{ $product->title }}"></a>
                                @endif

                            @php $i--; @endphp
                            @endforeach


                            <div class="card-body">
                              <h4 class="card-title">{{ $product->title}}</h4>
                              <p class="card-text">Taka: {{$product->price}} </p>
                              <a href=" {{ route('showSingleProduct', $product->slug)}} " class="btn btn-outline-primary mt-0 pt-0">Details</a>
                              <a href="#" class="btn btn-outline-primary mt-0 pt-0">Add to Cart</a>
                            </div>
                          </div>
                        </div>
                        {{-- product card end --}}
                    @endforeach

                    @else
                    <div class="justify-content-center mr-3">
                            <img class="card-img-top feature-image" src="{{ asset('images/empty.png')}}" alt="No Item" title="No item found">
                        </div>
                        <div class="justify-content-center mt-5">
                            <span class="h3">Sorry there are no item under this category!</span>
                            <p class="text-center h5">
                            <a href="{{ route('products') }}">But we do have some exiting items. Care to explore?</a></p>
                        </div>
                    @endif

              </div>

                <div class="row pagination d-flex align-items-center">
                    <div class="text-center">
                        {{$sortByCategory->links()}}
                    </div>
                </div>

            </div>
        </div>
    </div>

</div>



 {{-- End sidebar + content --}}


@endsection

