@extends('layouts/layout')

@section('title')

    {{ $product->title }} || LOREM Cart

@endsection

@section('content')
{{-- Start sidebar + content --}}

<div class="container-fluid ml-0 p-0 mt-20">
    <div class="row">
        <div class="col-md-3">

            <div id="carouselProductControls" class="carousel slide" data-ride="carousel">
              <div class="carousel-inner">

                @php $i = 1; @endphp

                @foreach ($product->images as $image)
                    <div class="carousel-item product-item-bg {{ $i == 1 ? 'active' : '' }}">
                      <img src="{{ asset('images/Products/'.$image->image)}}" class="d-block w-100" alt="{{$image->image}}">
                    </div>
                    @php $i++; @endphp

                @endforeach

          </div>
          <a class="carousel-control-prev" href="#carouselProductControls" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carouselProductControls" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>

</div>


<div class="col-md-9">
    <div class="widget">
      <h3 class="col-md-5"> {{$product->title}} </h3><hr/>
      <h3 class="ml-2"> <img class="mb-2" src="/images/coin_clear.png" width="25px" title="Price"> <span class="base-color">{{$product->price}}</span> Taka ||
        <span title="Stock" class="badge badge-pill pt-1 pb-1 {{ $product->quantity == 0 ? 'badge-danger' : 'badge-primary' }}">
        {{$product->quantity == 0 ? 'Out of Stock!' : ($product->quantity == 1 ? $product->quantity.' item in stock' : $product->quantity.' items in stock')}}
        </span>
        <img class="mb-1 mt-1" src="/images/cat_icon_3.svg" width="25px" title="Category" alt="Category Icon"> {{$product->category->name}} &nbsp;
        <img class="mb-1" src="/images/brand_icon_clear.png" width="25px" title="Brand" alt="Brand Icon"> {{$product->brand->name}}
      </h3>

  </div>
  <div class="product-description ml-2">
    {{$product->description}}
  </div>
</div>


</div>

</div>
{{-- End sidebar + content --}}

@endsection

