@extends('layouts/layout')

@section('title')

Search Results for {{$searchString}}

@endsection

@section('content')
 {{-- Start sidebar + content --}}

<div class="container-fluid ml-0 p-0 mt-20">
    <div class="row">
        <div class="col-md-3">
            @include('partials/productSidebar') {{-- sidebar inclution --}}
        </div>


        <div class="col-md-9">
            <div class="widget">
              <h3 class="h3 pb-2">Search Results for <span class="font-weight-bold badge badge-success pt-1 mt-1 justify-content-center">{{$searchString}}</span></h3><hr/>
              <div class="row">
                @if (count($products) > 0)
                    @foreach ($products as $product)
                        <div class="col-md-3 mr-2 mb-2">
                          <div class="card card-deck">

                            @php $i = 1; @endphp
                            @foreach ($product->images as $image)
                                @if ($i > 0)
                                <a href="{{ route('showSingleProduct', $product->slug)}}">
                                  <img class="card-img-top feature-image" src="{{ asset('images/Products/'.$image->image)}}" alt="{{ $product->title }}"></a>
                                @endif
                            @php $i--; @endphp
                            @endforeach


                            <div class="card-body">
                              <h4 class="card-title">{{ $product->title}}</h4>
                              <p class="card-text">Taka: {{$product->price}} </p>
                              <a href="{{ route('showSingleProduct', $product->slug)}}" class="btn btn-outline-primary mt-0 pt-0">Details</a>
                              <a href="#" class="btn btn-outline-primary mt-0 pt-0">Add to Cart</a>
                            </div>
                          </div>
                        </div>
                    @endforeach

                    @else
                        <div class="justify-content-center mr-3">
                            <img class="card-img-top feature-image" src="{{ asset('images/empty.png')}}" alt="No Item" title="No item found">
                        </div>
                        <div class="justify-content-center mt-5">
                            <span class="h3">Sorry there are no item related to your search!</span>
                            <p class="text-center h5">
                            <a href="{{ route('products') }}">But we do have some exiting items. Care to explore?</a></p>
                        </div>
                    @endif

              </div>

                <div class="pagination row d-flex align-items-center">
                    <div class="text-center">
                        {{$products->links()}}
                    </div>
                </div>


            </div>
        </div>


    </div>

</div>
 {{-- End sidebar + content --}}

@endsection
