@extends('layouts/adminLayout')

@section('title', 'Show category || LOREM Cart')


@section('content')

<div class="card">
    <div class="card-header text-center bg-primary">
        <h1>Category List</h1>
    </div>
    <div class="card-body">
        @include('partials/message')
        <table class="table table-hover table-striped">
            <tr class="text-center">
                <td>S.L.</td>
                <td>Category Name</td>
                <td>Category Image</td>
                <td>Parent Category</td>
                <td>Edit</td>
                <td>Delete</td>
            </tr>
            @php $i = 0; @endphp

            @foreach ($categories as $category)
                @php $i++ @endphp
                <tr class="text-center">
                    <td>{{$i}}</td>
                    <td>{{$category->name}}</td>
                    <td>
                        @if ($category->image)
                            <img src="{{ asset('images/Categories/'.$category->image) }}" width="100">
                        @endif
                    </td>
                    <td>
                        @if ($category->parent_id == NULL)
                            Parent Category
                        @else
                            {{$category->parent->name}}
                        @endif
                    </td>
                    <td><a class="btn btn-outline-primary" href="{{ route('editCategory', $category->id) }} ">Edit</a></td>
                    <td>
                        <!-- Button trigger modal -->
                        <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#deleteModal{{$category->id}}">
                          Delete
                        </button>

                        <!-- Modal -->
                        <div class="modal fade" id="deleteModal{{$category->id}}" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel" aria-hidden="true">
                          <div class="modal-dialog" role="document">
                            <div class="modal-content">
                              <div class="modal-header">
                                <h5 class="modal-title" id="deleteModalLabel">Sure to delete?</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                  <span aria-hidden="true">&times;</span>
                                </button>
                              </div>
                              <div class="modal-body text-center">
                                <form method="POST" action="{{ route('deleteCategory', $category->id) }}" enctype="multipart/form-data" >
                                    @csrf
                                    @method('DELETE')

                                    <button type="submit" class="btn btn-danger">Confirm Delete</button>
                                </form>
                              </div>
                              <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                              </div>
                            </div>
                          </div>
                        </div>
                    </td>
                </tr>
            @endforeach
        </table>
    </div>
</div>

@endsection
