@extends('layouts/adminLayout')

@section('title', 'Create new Category || LOREM Cart')


@section('content')

 <div class="card">
  <div class="card-header text-center bg-primary">
    <h1>Create new Category</h1>
  </div>
     <div class="card-body">
         <form data-parsley-validate action=" {{ route('storeCategory')}} " method="POST" enctype="multipart/form-data" >
            @csrf
            @include('partials/errors')
          <div class="form-group">
            <label for="categoryName">Category Name</label>
            <input type="text" class="form-control" id="categoryName" name="categoryName" placeholder="Category Name">
          </div>
          <div class="form-group">
            <label for="parentCategory">Parent Category (Optional)</label>
            <select class="form-control" id="parent_id" name="parent_id">
              <option value="">Make it Parent</option>
              @foreach ($mainCategory as $category)
                  <option value="{{$category->id}}">{{$category->name}}</option>
              @endforeach
            </select>
            </div>

          <div class="form-group">
                <label for="description">Category Description (Optional)</label>
                <textarea class="form-control" id="description" name="description" rows="3"></textarea>
          </div>
          <div class="form-row">
              <div class="input-file-container col-auto">
                <label for="categoryImage">Category Image</label>
                <input class="input-file" name="categoryImg" id="categoryImg" for="categoryImg" type="file" multiple>
                <label tabindex="0" name="categoryImg" id="categoryImg" for="categoryImg" class="input-file-trigger text-center">Select an Images</label>
              </div>
              <p class="file-return"></p>
          </div>


            <br/>
          <div class="form-row">
              <button type="submit" class="btn btn-primary">Submit</button>
              <button class="btn btn-light">Cancel</button>
          </div>
        </form>
     </div>
 </div>


@endsection


@section('scripts')


@endsection
