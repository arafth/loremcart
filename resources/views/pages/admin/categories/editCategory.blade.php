@extends('layouts/adminLayout')

@section('title', 'Edit Category || LOREM Cart')


@section('content')

 <div class="card">
  <div class="card-header text-center bg-primary">
    <h1>Edit Category</h1>
  </div>
     <div class="card-body">
         <form action=" {{ route('updateCategory', $id->id) }} " method="POST" enctype="multipart/form-data" >
            @method('PATCH')
            @csrf
            @include('partials/errors')
          <div class="form-group">
            <label for="categoryName">Category Name</label>
            <input type="text" class="form-control" id="categoryName" name="categoryName" value=" {{$id->name}} ">
          </div>
          <div class="form-group">
            <label for="parentCategory">Parent Category</label>
            <select class="form-control" id="parent_id" name="parent_id">
              @if ($id->parent_id == NULL)
                <option value="" selected>Parent Category</option>
              @else
              <option value="{{$id->parent->id}}" selected>{{$id->parent->name}}</option>
              <option value="">Make it Parent Category</option>
              @endif
              @foreach ($mainCategory as $category)
                  <option value="{{$category->id}}">{{$category->name}}</option>
              @endforeach
            </select>
            </div>

          <div class="form-group">
                <label for="description">Category Description</label>
                <textarea class="form-control" id="description" name="description" rows="3">{{$id->description}}</textarea>
          </div>
          <div class="form-row">
              <div class="input-file-container col-auto form-group">
                <label for="categoryImage">Category Image</label>
                <input class="input-file" name="categoryImg" id="categoryImg" for="categoryImg" type="file">
                <label tabindex="0" name="categoryImg" id="categoryImg" for="categoryImg" class="input-file-trigger text-center">
                  @if ($id->image)
                    Change Image
                  @else
                    Add Image
                  @endif

                </label>
              </div>
              <div class="col-auto form-group">
                @if ($id->image)
                  <img src="{{ asset('images/Categories/'.$id->image) }}" width="100">
                @endif
              </div>
              <p class="file-return"></p>
          </div>


            <br/>
          <div class="form-row">
              <button type="submit" class="btn btn-primary">Submit</button>
              <button class="btn btn-light">Cancel</button>
          </div>
        </form>
     </div>
 </div>


@endsection


@section('scripts')


@endsection
