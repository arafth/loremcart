@extends('layouts/adminLayout')

@section('title', 'Create new Category || LOREM Cart')


@section('content')

 <div class="card">
  <div class="card-header text-center bg-primary">
    <h1>Create new Division</h1>
  </div>
     <div class="card-body">
         <form data-parsley-validate action=" {{ route('storeDivision')}} " method="POST" enctype="multipart/form-data" >
            @csrf
            @include('partials/errors')
          <div class="form-group">
            <label for="name">Division Name</label>
            <input type="text" class="form-control" id="name" name="name" placeholder="Division Name">
          </div>

            <br/>
          <div class="form-row">
              <button type="submit" class="btn btn-primary">Submit</button>
              <button class="btn btn-light">Cancel</button>
          </div>
        </form>
     </div>
 </div>


@endsection


@section('scripts')


@endsection
