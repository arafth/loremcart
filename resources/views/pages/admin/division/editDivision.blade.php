@extends('layouts/adminLayout')

@section('title', 'Edit Category || LOREM Cart')


@section('content')

 <div class="card">
  <div class="card-header text-center bg-primary">
    <h1>Edit Division</h1>
  </div>
     <div class="card-body">
         <form action=" {{ route('updateDivision', $id->id) }} " method="POST" enctype="multipart/form-data" >
            @method('PATCH')
            @csrf
            @include('partials/errors')
          <div class="form-group">
            <label for="name">Division Name</label>
            <input type="text" class="form-control" id="name" name="name" value=" {{$id->name}} ">
          </div>

            <br/>
          <div class="form-row">
              <button type="submit" class="btn btn-primary">Submit</button>
              <button class="btn btn-light">Cancel</button>
          </div>
        </form>
     </div>
 </div>


@endsection
