@extends('layouts/adminLayout')

@section('title', 'Create new Brand || LOREM Cart')


@section('content')

 <div class="card">
  <div class="card-header text-center bg-primary">
    <h1>Create new Brand</h1>
  </div>
     <div class="card-body">
         <form data-parsley-validate action=" {{ route('storeBrand')}} " method="POST" enctype="multipart/form-data" >
            @csrf
            @include('partials/errors')
          <div class="form-group">
            <label for="brandName">Brand Name</label>
            <input type="text" class="form-control" id="brandName" name="brandName" placeholder="Brand Name">
          </div>

          <div class="form-group">
                <label for="description">Brand Description (Optional)</label>
                <textarea class="form-control" id="description" name="description" rows="3"></textarea>
          </div>
          <div class="form-row">
              <div class="input-file-container col-auto">
                <label for="brandImage">Brand Image</label>
                <input class="input-file" name="brandImg" id="brandImg" for="brandImg" type="file" multiple>
                <label tabindex="0" name="brandImg" id="brandImg" for="brandImg" class="input-file-trigger text-center">Select an Images</label>
              </div>
              <p class="file-return"></p>
          </div>


            <br/>
          <div class="form-row">
              <button type="submit" class="btn btn-primary">Submit</button>
              <button class="btn btn-light">Cancel</button>
          </div>
        </form>
     </div>
 </div>


@endsection


@section('scripts')


@endsection
