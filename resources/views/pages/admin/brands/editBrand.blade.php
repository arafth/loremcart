@extends('layouts/adminLayout')

@section('title', 'Edit Brand || LOREM Cart')


@section('content')

 <div class="card">
  <div class="card-header text-center bg-primary">
    <h1>Edit Brand</h1>
  </div>
     <div class="card-body">
         <form action=" {{ route('updateBrand', $id->id) }} " method="POST" enctype="multipart/form-data" >
            @method('PATCH')
            @csrf
            @include('partials/errors')
          <div class="form-group">
            <label for="brandName">Brand Name</label>
            <input type="text" class="form-control" id="brandName" name="brandName" value=" {{$id->name}} ">
          </div>

          <div class="form-group">
                <label for="description">Brand Description</label>
                <textarea class="form-control" id="description" name="description" rows="3">{{$id->description}}</textarea>
          </div>
          <div class="form-row">
              <div class="input-file-container col-auto form-group">
                <label for="brandImg">Brand Image</label>
                <input class="input-file" name="brandImg" id="brandImg" for="brandImg" type="file">
                <label tabindex="0" name="brandImg" id="brandImg" for="brandImg" class="input-file-trigger text-center">
                  @if ($id->image)
                    Change Image
                  @else
                    Add Image
                  @endif

                </label>
              </div>
              <div class="col-auto form-group">
                @if ($id->image)
                  <img src="{{ asset('images/Brands/'.$id->image) }}" width="100">
                @endif
              </div>
              <p class="file-return"></p>
          </div>


            <br/>
          <div class="form-row">
              <button type="submit" class="btn btn-primary">Submit</button>
              <button class="btn btn-light">Cancel</button>
          </div>
        </form>
     </div>
 </div>


@endsection


@section('scripts')


@endsection
