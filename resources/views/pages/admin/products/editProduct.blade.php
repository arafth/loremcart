@extends('layouts/adminLayout')

@section('title', 'Edit Product details || LOREM Cart')


@section('content')

    <div class="card">
        <div class="card-header text-center font-weight-bold">
            Edit {{$id->title}}'s informations
        </div>
     <div class="card-body">
        <form method="POST" action=" {{ route('updateProduct', $id->id)}} " enctype="multipart/form-data">
            @method('PATCH')
            @csrf

            @include('partials/errors')
          <div class="form-group">
            <label for="title">Product Title</label>
            <input type="text" class="form-control" id="title" name="title" value=" {{ $id->title }} ">
          </div>
          <div class="form-row">
            <div class="form-group col-md-6">
              <label for="price">Product Price</label>
              <input type="number" step="0.01" class="form-control" id="price" name="price" value={{$id->price}} >
            </div>
            <div class="form-group col-md-6">
              <label for="quantity">Product Quantity</label>
              <input type="number" class="form-control" id="quantity" name="quantity" value="{{$id->quantity}}" >
            </div>
          </div>
          <div class="form-row">
            {{-- <div class="form-group col-auto">
              <label for="category_id">Category ID</label>
              <input type="number" class="form-control" id="category_id" name="category_id" value="{{$id->category_id}}">
            </div>
            <div class="form-group col-auto">
              <label for="brand_id">Brand ID</label>
              <input type="number" class="form-control" id="brand_id" name="brand_id" value="{{$id->brand_id}}">
            </div> --}}
            <div class="form-group col-auto">
                    <label for="category_id">Select Category</label>
                    <select class="form-control" id="category_id" name="category_id">
                        <option value="{{$id->category_id}} " selected>{{$id->category->name}} </option>
                        @foreach (App\Models\Category::orderBy('name', 'asc')->where('parent_id', null)->get() as $parent)
                        <option value="{{$parent->id}}">{{$parent->name}} </option>
                        @foreach (App\Models\Category::orderBy('name', 'asc')->where('parent_id', $parent->id)->get() as
                        $child)
                        <option value="{{$child->id}} ">------>{{$child->name}} </option>
                        @endforeach
                        @endforeach
                    </select>
                </div>

                <div class="form-group col-auto">
                    <label for="brand_id">Brand ID</label>
                    <select class="form-control" id="brand_id" name="brand_id">
                        <option value="{{$id->brand_id}} " selected>{{$id->brand->name}} </option>
                    @foreach (App\Models\Brand::orderBy('id', 'asc')->get() as $brand)
                        <option value="{{$brand->id}} ">{{$brand->name}} </option>
                    @endforeach
                    </select>
                </div>
            <div class="form-group col-auto">
              <label for="admin_id">Admin ID</label>
              <input type="number" class="form-control" id="admin_id" name="admin_id" value="{{$id->admin_id}}" readonly>
            </div>
          </div>
          <div class="form-group">
                <label for="description">Product Description</label>
                <textarea class="form-control" id="description" name="description" rows="3"> {{ $id->description }} </textarea>
          </div>
          <div class="form-row">
              <div class="input-file-container col-auto">
                <input class="input-file" name="ProductImg[]" id="ProductImg" for="ProductImg" type="file" multiple>
                <label tabindex="0" name="ProductImg[]" id="ProductImg" for="ProductImg" class="input-file-trigger text-center">Select Images</label>
              </div>
              <p class="file-return"></p>
          </div>


            <br/>
          <div class="form-row">
              <button type="submit" class="btn btn-primary">Submit</button>
              <button class="btn btn-light">Cancel</button>
          </div>
        </form>
     </div>
 </div>

@endsection
