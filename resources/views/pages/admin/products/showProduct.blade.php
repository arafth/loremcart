@extends('layouts/adminLayout')

@section('title', 'Show product list || LOREM Cart')


@section('content')

<div class="card">
    <div class="card-header text-center bg-primary">
        <h1>Product List</h1>
    </div>
    <div class="card-body">
        @include('partials/message')
        <table class="table table-hover table-striped">
            <tr class="text-center">
                <td>S.L.</td>
                <td>Product Title</td>
                <td>Price</td>
                <td>Quantity</td>
                <td>Category ID</td>
                <td>Brand ID</td>
                <td>Admin ID</td>
                <td>Edit</td>
                <td>Delete</td>
            </tr>
            @php $i = 0; @endphp

            @foreach ($products as $product)
                @php $i++ @endphp
                <tr class="text-center">
                    <td>{{$i}}</td>
                    <td>{{$product->title}}</td>
                    <td>{{$product->price}}</td>
                    <td>{{$product->quantity}}</td>
                    <td>{{$product->category_id}}</td>
                    <td>{{$product->brand_id}}</td>
                    <td>{{$product->admin_id}}</td>
                    <td><a class="btn btn-outline-primary" href="{{ route('editProduct', $product->id) }} ">Edit</a></td>
                    <td>
                        <!-- Button trigger modal -->
                        <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#deleteModal{{$product->id}}">
                          Delete
                        </button>

                        <!-- Modal -->
                        <div class="modal fade" id="deleteModal{{$product->id}}" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel" aria-hidden="true">
                          <div class="modal-dialog" role="document">
                            <div class="modal-content">
                              <div class="modal-header">
                                <h5 class="modal-title" id="deleteModalLabel">Sure to delete?</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                  <span aria-hidden="true">&times;</span>
                                </button>
                              </div>
                              <div class="modal-body text-center">
                                <form method="POST" action="{{ route('deleteProduct', $product->id) }}" enctype="multipart/form-data" >
                                    @csrf
                                    @method('DELETE')

                                    <button type="submit" class="btn btn-danger">Confirm Delete</button>
                                    {{-- <a class="btn btn-danger" href="">Confirm Delete</a> --}}
                                </form>
                              </div>
                              <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                              </div>
                            </div>
                          </div>
                        </div>
                    </td>
                </tr>
            @endforeach
        </table>
    </div>
</div>

@endsection
