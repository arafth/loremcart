@extends('layouts/adminLayout')

@section('title', 'Show category || LOREM Cart')


@section('content')

<div class="card">
    <div class="card-header text-center bg-primary">
        <h1>District List</h1>
    </div>
    <div class="card-body">
        @include('partials/message')
        <table class="table table-hover table-striped">
            <tr class="text-center">
                <td>S.L.</td>
                <td>District Name</td>
                <td>Division Name</td>
                <td>Edit</td>
                <td>Delete</td>
            </tr>
            @php $i = 0; @endphp

            @foreach ($districts as $district)
                @php $i++ @endphp
                <tr class="text-center">
                    <td>{{$i}}</td>
                    <td>{{$district->name}}</td>
                    <td>{{$district->division->name}}</td>
                    <td><a class="btn btn-outline-primary" href="{{ route('editDistrict', $district->id) }} ">Edit</a></td>
                    <td>
                        <!-- Button trigger modal -->
                        <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#deleteModal{{$district->id}}">
                          Delete
                        </button>

                        <!-- Modal -->
                        <div class="modal fade" id="deleteModal{{$district->id}}" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel" aria-hidden="true">
                          <div class="modal-dialog" role="document">
                            <div class="modal-content">
                              <div class="modal-header">
                                <h5 class="modal-title" id="deleteModalLabel">Sure to delete?</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                  <span aria-hidden="true">&times;</span>
                                </button>
                              </div>
                              <div class="modal-body text-center">
                                <form method="POST" action="{{ route('deleteDistrict', $district->id) }}" enctype="multipart/form-data" >
                                    @csrf
                                    @method('DELETE')

                                    <button type="submit" class="btn btn-danger">Confirm Delete</button>
                                </form>
                              </div>
                              <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                              </div>
                            </div>
                          </div>
                        </div>
                    </td>
                </tr>
            @endforeach
        </table>
    </div>
</div>

@endsection
