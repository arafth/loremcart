@extends('layouts/adminLayout')

@section('title', 'Edit Category || LOREM Cart')


@section('content')

 <div class="card">
  <div class="card-header text-center bg-primary">
    <h1>Edit District</h1>
  </div>
     <div class="card-body">
         <form action=" {{ route('updateDistrict', $district->id) }} " method="POST" enctype="multipart/form-data" >
            @method('PATCH')
            @csrf
            @include('partials/errors')
          <div class="form-group">
            <label for="name">District Name</label>
            <input type="text" class="form-control" id="name" name="name" value=" {{$district->name}} ">
          </div>
          <div class="form-group">
            <label for="DivisionName">Parent Category</label>
            <select class="form-control" id="division_id" name="division_id">
                <option value="{{$district->division_id}} " selected>{{$district->division->name}} </option>
            @foreach ($divisions as $division)
                <option value="{{$division->id}}">{{$division->name}}</option>
            @endforeach
            </select>
            </div>

            <br/>
          <div class="form-row">
              <button type="submit" class="btn btn-primary">Submit</button>
              <button class="btn btn-light">Cancel</button>
          </div>
        </form>
     </div>
 </div>


@endsection
