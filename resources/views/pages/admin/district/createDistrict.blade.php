@extends('layouts/adminLayout')

@section('title', 'Create new Category || LOREM Cart')


@section('content')

 <div class="card">
  <div class="card-header text-center bg-primary">
    <h1>Create new District</h1>
  </div>
     <div class="card-body">
         <form data-parsley-validate action=" {{ route('storeDistrict')}} " method="POST" enctype="multipart/form-data" >
            @csrf
            @include('partials/errors')
          <div class="form-group">
            <label for="name">District Name</label>
            <input type="text" class="form-control" id="name" name="name" placeholder="District Name">
          </div>
          <div class="form-group">
            <label for="parentCategory">Division</label>
            <select class="form-control" id="division_id" name="division_id">
              <option value="">Please select a Division</option>
              @foreach ($divisions as $division)
                  <option value="{{$division->id}}">{{$division->name}}</option>
              @endforeach
            </select>
            </div>

            <br/>
          <div class="form-row">
              <button type="submit" class="btn btn-primary">Submit</button>
              <button class="btn btn-light">Cancel</button>
          </div>
        </form>
     </div>
 </div>


@endsection


@section('scripts')


@endsection
