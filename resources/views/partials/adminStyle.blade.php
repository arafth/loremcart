<!-- plugins:css -->
<link rel="stylesheet" href=" {{ asset('/AdminAsset/vendors/iconfonts/mdi/css/materialdesignicons.min.css') }} ">
<link rel="stylesheet" href="{{ asset('/AdminAsset/vendors/iconfonts/ionicons/css/ionicons.css') }}">
<link rel="stylesheet" href=" {{asset('/AdminAsset/vendors/iconfonts/typicons/src/font/typicons.css') }}">
<link rel="stylesheet" href=" {{ asset('/AdminAsset/vendors/iconfonts/flag-icon-css/css/flag-icon.min.css')}}">
<link rel="stylesheet" href=" {{ asset('/AdminAsset/vendors/css/vendor.bundle.base.css')}}">
<link rel="stylesheet" href="{{ asset('/AdminAsset/vendors/css/vendor.bundle.addons.css')}}">
<!-- endinject -->
<!-- plugin css for this page -->
<!-- End plugin css for this page -->
<!-- inject:css -->
<link rel="stylesheet" href="{{ asset('/AdminAsset/css/shared/style.css')}}">
<!-- endinject -->
<!-- Layout styles -->
<link rel="stylesheet" href="{{ asset('/AdminAsset/css/adminCss/style.css')}}">
<!-- End Layout styles -->
<link rel="shortcut icon" href=" {{ asset('images/favicon.ico')}}" />
<link rel="shortcut icon" href=" {{ asset('css/select2.min.css')}}" />
<link href="https://fonts.googleapis.com/css?family=Nunito|Open+Sans&display=swap" rel="stylesheet">



{{-- debug css --}}
{{-- <link rel="stylesheet" type="text/css" href="{{ asset('css/debug.css')}}"> --}}
