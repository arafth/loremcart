<link rel="stylesheet" type="text/css" href="{{ asset('bootstrap/css/bootstrap.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('css/style.css') }}">
<link href="https://fonts.googleapis.com/css?family=Nunito|Open+Sans&display=swap" rel="stylesheet">

{{-- ##################  css for debuging  ##################  --}}
{{-- <link rel="stylesheet" type="text/css" href="{{ asset('css/debug.css')}}"> --}}
