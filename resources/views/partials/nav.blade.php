{{-- start navbar --}}

<nav class="navbar navbar-expand-lg nav-fill m-auto ml-0 mr-0 navbar_custom ml-5">
  <a class="navbar-brand" href="{{ route('index') }}"><figure>
      <img src="{{ asset('images/ecom_logo.png')}}" style="max-width: 128px;">
  </figure></a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
</button>

<div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item active">
        <a class="nav-link navmenu" href=" {{ route('index') }} ">Home <span class="sr-only">(current)</span></a>
    </li>

    <li class="nav-item">
        <a class="nav-link navmenu" href=" {{ route('products') }} ">Products</a>
    </li>

    <li class="nav-item">
        <a class="nav-link navmenu" href=" {{ route('contact') }} ">Contact</a>
    </li>
      {{-- <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle navmenu" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Dropdown
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="#">Action</a>
          <a class="dropdown-item" href="#">Another action</a>
          <div class="dropdown-divider"></div>
          <a class="dropdown-item" href="#">Something else here</a>
        </div>
    </li> --}}
</ul>

<form class="form-inline my-2 my-lg-0" action=" {{ route('search') }} " method="GET">
    <div class="container h-100">
      <div class="d-flex justify-content-center h-100">
        <div class="searchbar m-0 p-0">
          <input class="search_input mt-0" type="text" name="search" placeholder="   Search">
          <button class="search_icon" type="submit"><i class="fa fa-search"></i></button>
      </div>
  </div>
</div>
</form>

<ul>
    {{-- user login and register --}}

    <div class="container">

      <button class="navbar-toggler navbar_custom" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <!-- Left Side Of Navbar -->
        <ul class="navbar-nav mr-auto">

        </ul>
        <!-- Right Side Of Navbar -->
        <ul class="navbar-nav ml-auto">
            <!-- Authentication Links -->
            @guest
            <li class="nav-item">
                <a class="nav-link navbar_custom" href="{{ route('login') }}">{{ __('Login') }}</a>
            </li>
            @if (Route::has('register'))
            <li class="nav-item">
                <a class="nav-link navbar_custom" href="{{ route('register') }}">{{ __('Register') }}</a>
            </li>
            @endif
            @else
            <li class="nav-item dropdown">
                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                    {{ Auth::user()->name }} <span class="caret"></span>
                </a>

                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                    <a class="dropdown-item" href="{{ route('logout') }}"
                    onclick="event.preventDefault();
                    document.getElementById('logout-form').submit();">
                    {{ __('Logout') }}
                </a>

                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    @csrf
                </form>
            </div>
        </li>
        @endguest
    </ul>
</div>
</div>
</ul>



</div>
</nav>
{{-- End navbar --}}
