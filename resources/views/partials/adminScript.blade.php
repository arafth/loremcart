<!-- container-scroller -->
<!-- plugins:js -->
<script  src=" {{ asset('/AdminAsset/vendors/js/vendor.bundle.base.js')}}"></script>
<script  src=" {{ asset('/AdminAsset/vendors/js/vendor.bundle.addons.js')}}"></script>
<!-- endinject -->
<!-- Plugin js for this page-->
<!-- End plugin js for this page-->
<!-- inject:js -->
<script  src=" {{ asset('/AdminAsset/js/shared/off-canvas.js')}}"></script>
<script  src=" {{ asset('/AdminAsset/js/shared/misc.js')}}"></script>
<!-- endinject -->
<!-- Custom js for this page-->
<script  src="{{ asset('/AdminAsset/js/admin/dashboard.js')}}"></script>
<!-- End custom js for this page-->
<!-- font awesome inclution -->
<script  type="text/javascript" src="{{ asset('FontAwesome/js/all.js') }}"></script>
  {{-- Custom Script for admin --}}
<script  src="{{ asset('/AdminAsset/js/customAdminScript.js')}}"></script>

<script  src="{{ asset('bootstrap/js/bootstrap.min.js') }}"></script>

<script src="{{ asset('js/jquery.min.js') }}"></script>
<script  src="{{ asset('js/parsley.min.js') }}"></script>
<script  src="{{ asset('js/select2.min.js') }}"></script>
