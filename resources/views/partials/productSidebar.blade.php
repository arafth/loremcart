<div class="list-group">
    @foreach (App\Models\Category::orderBy('name', 'asc')->where('parent_id', null)->get() as $parent)

    <a href="#main-{{$parent->id}} " class="list-group-item list-group-item-action" data-toggle='collapse'>
        <img src="{{ asset('images/Categories/'.$parent->image) }}" width="30px">
        {{$parent->name}}</a>
        <div class="collapse
        @if (Route::is('showCategoryWiseProducts'))
            @php $slugID = App\Models\Category::where('slug', $slug)->value('id');
            $sortByCategory = App\Models\Product::where('category_id', $slugID)->first();
            @endphp
{{--             @if($sortByCategory->category_id == null)
                '' --}}
            @if ($parent->parentOrNotCategory($parent->id, $sortByCategory->category_id))
                show
            @endif
        @endif
        " id="main-{{ $parent->id }}">
            <div class="child-rows">
                @foreach (App\Models\Category::orderBy('name', 'asc')->where('parent_id', $parent->id)->get() as $child)

                    <a href="{{ route('showCategoryWiseProducts', $child->slug) }}" class="list-group-item list-group-item-action
                    @if (Route::is('showCategoryWiseProducts'))
                        @if ($child->id == $sortByCategory->category_id)
                            active
                        @endif
                    @endif
                    ">
                    <img src="{{ asset('images/Categories/'.$child->image) }}" width="25px">
                    {{$child->name}}
                </a>
                @endforeach
            </div>
        </div>
        @endforeach
</div>
