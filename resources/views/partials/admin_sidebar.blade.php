<nav class="sidebar sidebar-offcanvas" id="sidebar">
    <ul class="nav">
        <li class="nav-item nav-profile">
            <a href="#" class="nav-link">
                <div class="profile-image">
                    <img class="img-xs rounded-circle" src="{{ asset('/images/ecom_logo.png')}}" alt="profile image">
                    <div class="dot-indicator bg-success"></div>
                </div>
                <div class="text-wrapper text-center">
                    <p class="profile-name">User Name</p>
                    <p class="designation">Admin</p>
                </div>
            </a>
        </li>
        <li class="nav-item nav-category">Main Menu</li>
        <li class="nav-item">
            <a class="nav-link" href="{{ route('dashboard')}}">
                <i class="menu-icon typcn typcn-document-text"></i>
                <span class="menu-title">Dashboard</span>
            </a>
        </li>

        {{-- collapsable menu start --}}
        <li class="nav-item">
            <a class="nav-link" data-toggle="collapse" href="#authProduct" aria-expanded="false" aria-controls="authProduct">
                <i class="menu-icon typcn typcn-document-add"></i>
                <span class="menu-title">Manage Products</span>
                <i class="menu-arrow"></i>
            </a>
            <div class="collapse" id="authProduct">
                <ul class="nav flex-column sub-menu">
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('createProduct')}}"> Create Product </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('showProductList') }}"> Product List </a>
                    </li>

                    {{--                   <li class="nav-item">
                    <a class="nav-link" href="pages/samples/register.html"> Register </a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" href="pages/samples/error-404.html"> 404 </a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" href="pages/samples/error-500.html"> 500 </a>
                  </li> --}}
                </ul>
            </div>
        </li>
        <li class="nav-item">
            <a class="nav-link" data-toggle="collapse" href="#authCategory" aria-expanded="false"
                aria-controls="authCategory">
                <i class="menu-icon typcn typcn-document-add"></i>
                <span class="menu-title">Manage Category</span>
                <i class="menu-arrow"></i>
            </a>
            <div class="collapse" id="authCategory">
                <ul class="nav flex-column sub-menu">
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('createCategory')}}"> Create Category </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('showCategory') }}"> Category List </a>
                    </li>
                </ul>
            </div>
        </li>

        <li class="nav-item">
            <a class="nav-link" data-toggle="collapse" href="#authBrand" aria-expanded="false"
                aria-controls="authBrand">
                <i class="menu-icon typcn typcn-document-add"></i>
                <span class="menu-title">Manage Brand</span>
                <i class="menu-arrow"></i>
            </a>
            <div class="collapse" id="authBrand">
                <ul class="nav flex-column sub-menu">
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('createBrand')}}"> Create Brand </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('showBrand') }}"> Brand List </a>
                    </li>
                </ul>
            </div>
        </li>

                <li class="nav-item">
            <a class="nav-link" data-toggle="collapse" href="#authDivision" aria-expanded="false"
                aria-controls="authDivision">
                <i class="menu-icon typcn typcn-document-add"></i>
                <span class="menu-title">Manage Division</span>
                <i class="menu-arrow"></i>
            </a>
            <div class="collapse" id="authDivision">
                <ul class="nav flex-column sub-menu">
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('createDivision')}}"> Create Division </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('showDivision') }}"> Division List </a>
                    </li>
                </ul>
            </div>
        </li>

        <li class="nav-item">
            <a class="nav-link" data-toggle="collapse" href="#authDistrict" aria-expanded="false"
                aria-controls="authDistrict">
                <i class="menu-icon typcn typcn-document-add"></i>
                <span class="menu-title">Manage District</span>
                <i class="menu-arrow"></i>
            </a>
            <div class="collapse" id="authDistrict">
                <ul class="nav flex-column sub-menu">
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('createDistrict')}}"> Create District </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('showDistrict') }}"> District List </a>
                    </li>
                </ul>
            </div>
        </li>



        {{--             <li class="nav-item">
              <a class="nav-link" href="pages/charts/chartjs.html">
                <i class="menu-icon typcn typcn-th-large-outline"></i>
                <span class="menu-title">Charts</span>
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="pages/tables/basic-table.html">
                <i class="menu-icon typcn typcn-bell"></i>
                <span class="menu-title">Tables</span>
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="pages/icons/font-awesome.html">
                <i class="menu-icon typcn typcn-user-outline"></i>
                <span class="menu-title">Icons</span>
              </a>
            </li> --}}

        {{--             <li class="nav-item">
              <a class="nav-link" data-toggle="collapse" href="#auth" aria-expanded="false" aria-controls="auth">
                <i class="menu-icon typcn typcn-document-add"></i>
                <span class="menu-title">User Pages</span>
                <i class="menu-arrow"></i>
              </a>
              <div class="collapse" id="auth">
                <ul class="nav flex-column sub-menu">
                  <li class="nav-item">
                    <a class="nav-link" href="pages/samples/blank-page.html"> Blank Page </a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" href="pages/samples/login.html"> Login </a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" href="pages/samples/register.html"> Register </a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" href="pages/samples/error-404.html"> 404 </a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" href="pages/samples/error-500.html"> 500 </a>
                  </li>
                </ul>
              </div>
            </li> --}}
    </ul>
</nav>
